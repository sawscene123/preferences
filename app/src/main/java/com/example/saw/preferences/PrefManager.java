package com.example.saw.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by saw on 12/18/2017.
 */

public class PrefManager {
    public static final String PREF_LOGIN = "PREF_LOGIN";// static constant variable
    public static final String PREF_SETTINGS = "PREF_USER_DETAIL";

    public static final String KEY_USERNAME = "USERNAME";
    private static final String KEY_PASSWORD = "PASSWORD";
    private static final String KEY_EARNED_AMOUNT = "KEY_EARNED_AMOUNT";
    private SharedPreferences prefs;

    public PrefManager(Context context, String prefName) {
//        prefs = ((Activity) context).getPreferences(Context.MODE_PRIVATE); // saves value in android's default preference file
        prefs = context.getSharedPreferences("PREF_LOGIN", Context.MODE_PRIVATE); // creates your own preference file-> PREF_LOGIN
    }

    public void setUsername(String username) {
        SharedPreferences.Editor editor = prefs.edit(); // to get editor object of that preferences
        editor.putString(KEY_USERNAME, username); // to make data-value ready
        editor.apply(); // to write above data-value in pref file.
    }

    public void setEarnedAmount(double earnedAmount) {//10.20
        prefs.edit()//editor object
                .putString(KEY_EARNED_AMOUNT, String.valueOf(earnedAmount))//"10.20" data-value ready
                .apply(); //write
    }

    public double getEarnedAmount() {
        String amount = prefs.getString(KEY_EARNED_AMOUNT, "0");

        return Double.parseDouble(amount);
    }

    public String getUsername() {
        return prefs.getString(KEY_USERNAME, "abcdef");
    }

    public String getPassword() {
        return prefs.getString(KEY_PASSWORD, "abcdef");
    }

    public void setPassword(String password) {
        prefs.edit()
                .putString(KEY_PASSWORD, password)
                .apply();
    }

    public void removeUsername() {
        prefs.edit()
                .remove(KEY_USERNAME) //remove only given key-value data -> USERNAME
                .apply();
    }

    public void clearAll() {
        prefs.edit()
                .clear() // removes all key-value data
                .apply();
    }
/*
PASSWORD = "PASSWORD";
 */
}
