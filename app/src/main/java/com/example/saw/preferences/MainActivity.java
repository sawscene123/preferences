package com.example.saw.preferences;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText etUsername, etPassword;
    private PrefManager prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUi();
//      SharedPreferences prefs = getPreferences(MODE_PRIVATE);
        prefs = new PrefManager(this, PrefManager.PREF_LOGIN);

        String username = prefs.getUsername();
        String password = prefs.getPassword();

        etUsername.setText(username);
        etPassword.setText(password);
    }

    private void initUi() {
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        findViewById(R.id.btn_login).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
                prefs.setUsername(username);
                prefs.setPassword(password);
                break;
        }
    }
}
